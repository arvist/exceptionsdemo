package demo;

import java.time.LocalDate;
import java.util.Objects;

public class Person /*implements Comparable<Person>*/ {

    private String firstName;
    private String lastName;
    private String personalCode;
    private LocalDate dateOfBirth;

    public Person(String firstName, String lastName, String personalCode, LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.personalCode = personalCode;
        this.dateOfBirth = dateOfBirth;
    }

    public Person(String firstName, String lastName, String personalCode) {
        this(firstName, lastName, personalCode, null);
    }

    //TODO
    //compare
    //hashcode
    //equals

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", personalCode='" + personalCode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(personalCode, person.personalCode) &&
                Objects.equals(dateOfBirth, person.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, personalCode, dateOfBirth);
    }
}

