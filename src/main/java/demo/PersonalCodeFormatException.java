package demo;

public class PersonalCodeFormatException extends Exception {


    public PersonalCodeFormatException(String s) {
        super(s);
    }

    public PersonalCodeFormatException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
