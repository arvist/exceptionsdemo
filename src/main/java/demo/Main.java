package demo;

public class Main {

    public static void main(String[] args) {


        Person p1 = new Person("John", "Doe", "290190-10000");
        Person p2 = new Person("Jane ", "Doe", "290190-10000");
        Person p3 = new Person("Arthur ", "Doe", "29019010000");
        Person p4 = new Person("Cnut", "Doe", "290190-10000");

        Person[] personArray = new Person[]{p1, p2, null, p4};

        for (int i = 0; i < 4; i++) {

            try {
                validatePerson(personArray[i]);
                System.out.println(personArray[i].getFirstName() + " Valid");
            } catch (PersonalCodeFormatException e) {
                System.out.println("Got PersonalCodeFormatException " + e.getMessage());
                //Fix personal code by inserting - after first 6 digits
            }
        }
    }

    public static void validatePerson(Person person) throws PersonalCodeFormatException {
        boolean isFormatCorrect = person.getPersonalCode().contains("-");
        if (isFormatCorrect == false) {
            throw new PersonalCodeFormatException("Incorrect format");
        }
    }


}
